package hello;

import java.util.Date;
import java.util.Calendar;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by romankovrigin on 04.04.16.
 */
public class ShopInfo {
    @JsonView
    public String Name;
    @JsonView
    public String Info;
    @JsonView
    public String AdvType;
    @JsonView
    public String header;
    @JsonView
    public String imageUrl;
    @JsonView
    public String banerUrl;
    @JsonView
    public Date begin;
    @JsonView
    public Date end;
    @JsonView
    public String address;
    @JsonView
    public String website;
    public ShopInfo(String name, String info, String header, String imageUrl, String banerUrl, String advType)
    {
        this.Name = name;
        this.Info = info;
        this.AdvType = advType;
        this.header = header;
        this.imageUrl = imageUrl;
        this.banerUrl = banerUrl;
        Calendar calendar = Calendar.getInstance();
        this.begin = calendar.getTime();
        calendar.add(Calendar.YEAR, 1);
        this.end = calendar.getTime();
    }
}
