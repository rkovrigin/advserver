package hello.Figures;

import hello.DBMS.SQliteManager;
import hello.ShopInfo;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * Created by romankovrigin on 04.04.16.
 */
public class RKPoint implements Figure{
    @JsonView
    public Coordinates point;
    @JsonView
    public double radius;
    @JsonView
    public final String type;
    @JsonView
    public ShopInfo shopInfo;
    public static Exception e = new Exception("Invalid radius size!");

    public RKPoint(double latitude, double longitude, double radius, ShopInfo shopInfo) throws Exception
    {
        if (radius < 0)
            throw e;
        this.point = new Coordinates(latitude, longitude);
        this.radius = radius;
        this.shopInfo = shopInfo;
        type = "point";
    }

    @Override
    public void saveToDB() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            mapper.disable(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS);
            String figure = mapper.writeValueAsString(this);
            System.out.println("saveToDB:" + figure);
            SQliteManager sQliteManager = SQliteManager.getInstance();
            sQliteManager.addCoord(figure, type);
        }catch (Exception e) {
            System.out.println("Error in saveToDB: " + e.toString());
        }
    }
}
