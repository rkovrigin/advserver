package hello.Figures;

import hello.DBMS.SQliteManager;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * Created by romankovrigin on 22.03.16.
 */
public class RKPolygon implements Figure {
    @JsonView
    public ArrayList<Coordinates> coords;
    @JsonView
    public final String type;
    public static Exception listIsNull = new Exception("List is NULL in constructor!");
    public static Exception invalidCoord = new Exception("Invalid coord!");
    public static Exception invalidValue = new Exception("Invalid value in coord pair!");

    public RKPolygon(String list) throws Exception
    {
        if (list.isEmpty())
            throw listIsNull;
        this.type = "polygon";
        this.coords = new ArrayList<Coordinates>();
        String[] pairs = list.split(";");
        for (int i = 0; i < pairs.length; ++i)
        {
            System.out.println(pairs[i]);
            if (!pairs[i].contains(":"))
                throw invalidCoord;
            String[] pair = pairs[i].split(":");
            if (pair.length != 2)
                throw invalidCoord;
            try
            {
                Coordinates coord = new Coordinates(Double.parseDouble(pair[0]), Double.parseDouble(pair[1]));
                coords.add(coord);
            }
            catch (Exception e)
            {
                System.out.println(e);
                throw invalidValue;
            }
        }
    }

    @Override
    public void saveToDB() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            String figure = mapper.writeValueAsString(this);
            System.out.println("saveToDB:" + figure);
            SQliteManager sQliteManager = SQliteManager.getInstance();
            sQliteManager.addCoord(figure, type);
        }catch (Exception e) {
            System.out.println("Error in saveToDB: " + e.toString());
        }
    }
}
