package hello.Figures;

import java.awt.*;
import java.awt.geom.Point2D;
import com.fasterxml.jackson.annotation.JsonView;
import hello.DBMS.SQliteManager;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 * Created by romankovrigin on 12.03.16.
 */
public class RKRectangle implements Figure {
    @JsonView
    public Coordinates leftbottom;
    @JsonView
    public Coordinates righttop;
    @JsonView
    public final String type;

    public RKRectangle(double lat1, double long1, double lat2, double long2) {
        this.leftbottom = new Coordinates(lat1, long1);
        this.righttop = new Coordinates(lat2, long2);
        this.type = "rect";
    }

    @Override
    public void saveToDB() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            String figure = mapper.writeValueAsString(this);
            System.out.println("saveToDB:" + figure);
            SQliteManager sQliteManager = SQliteManager.getInstance();
            sQliteManager.addCoord(figure, type);
        }catch (Exception e) {
            System.out.println("Error in saveToDB: " + e.toString());
        }
    }
}
