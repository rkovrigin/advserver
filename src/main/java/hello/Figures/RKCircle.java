package hello.Figures;

/**
 * Created by romankovrigin on 22.03.16.
 */
import hello.DBMS.SQliteManager;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;

public class RKCircle implements Figure {
    @JsonView
    public Coordinates point;
    @JsonView
    public double radius;
    @JsonView
    public final String type;
    public static Exception e = new Exception("Invalid radius size!");

    public RKCircle(double latitude, double longitude, double radius) throws Exception
    {
        if (radius < 0)
            throw e;
        this.point = new Coordinates(latitude, longitude);
        this.radius = radius;
        type = "circle";
    }

    @JsonIgnore
    public String getJsonString() throws Exception
    {
        String ret = new String();
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            ret = mapper.writeValueAsString(this);
        }catch (Exception e)
        {
            System.out.println(e.toString());
        }

        return ret;
    }

    @JsonIgnore
    @Override
    public void saveToDB() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            String figure = mapper.writeValueAsString(this);

//            System.out.println("saveToDB:" + figure);
//            SQliteManager sQliteManager = SQliteManager.getInstance();
//            sQliteManager.addCoord(figure, type);
        }catch (Exception e) {
            System.out.println("Error in saveToDB: " + e.toString());
        }
    }

}
