package hello.Figures;

/**
 * Created by romankovrigin on 22.03.16.
 */

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;

public class Coordinates {
    @JsonView
    public double latitude;
    @JsonView
    public double longitude;
    public Coordinates(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
