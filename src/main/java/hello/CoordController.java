package hello;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import hello.Adv.*;
import hello.DBMS.MongoDBManager;
import hello.Figures.*;
import hello.DBMS.SQliteManager;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;


/**
 * Created by romankovrigin on 09.03.16.
 */

@RestController
public class CoordController {
    @RequestMapping("/figures")
    public ArrayList<String> getCoords() throws SQLException{
        ArrayList<String> figures = null;
        SQliteManager sqlite = SQliteManager.getInstance();
        figures = sqlite.selectAll();
        return figures;
    }

    @RequestMapping("/malls/{cityid}") // __2__
    public ArrayList<Document> getMalls(@PathVariable("cityid") String cityid) throws SQLException {
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        return mongoDBManager.getMalls(cityid);
    }

    @RequestMapping("/adv/city/{cityid}") //__3__
    public ArrayList<Document> getAdvs(@PathVariable("cityid") String cityid)throws SQLException{
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        return mongoDBManager.getAdvertsAtCity(cityid, "");
    }

    @RequestMapping("/adv/city/{cityid}/mall/{mallid}") //__4__
    public ArrayList<Document> getAdvs(@PathVariable("cityid") String cityid,
                                       @PathVariable("mallid") String mallid) throws SQLException
    {
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        return mongoDBManager.getAdvertsAtCity(cityid, mallid);
    }

    @RequestMapping("/cities") //__1__
    public ArrayList<Document> getCities()throws SQLException{
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        return mongoDBManager.getCities();
    }

    @RequestMapping("/mongotest")
    public void mongotest() throws Exception {
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
//        mongoDBManager.getAdvMall(1022563);
//        RKCircle rkc = new RKCircle(100, 100, 100);
//        rkc.saveToDB();
//        String jsonString = rkc.getJsonString();
//        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
//        mongoDBManager.saveObject(jsonString);
    }

    @RequestMapping("/polygons")
    public ArrayList<String> getPolygons() throws SQLException
    {
        ArrayList<String> figures = null;
        SQliteManager sqlite = SQliteManager.getInstance();
        figures = sqlite.selectQuery("SELECT * FROM coord WHERE type is 'polygon'");
        return figures;
    }

    @RequestMapping("/rect/add")
    public void add(@RequestParam(value="lat1") double lat1,
                    @RequestParam(value="long1") double long1,
                    @RequestParam(value="lat2") double lat2,
                    @RequestParam(value="long2") double long2)
    {
        RKRectangle rkRectangle = new RKRectangle(lat1, long1, lat2, long2);
        rkRectangle.saveToDB();
    }

    @RequestMapping("/circle/add")
    public void add(@RequestParam(value="latitude") double latitude,
                    @RequestParam(value="longitude") double longitude,
                    @RequestParam(value="radius") double radius)
    {
        try{
            RKCircle circle = new RKCircle(latitude, longitude, radius);
            circle.saveToDB();
        }catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    @RequestMapping("/polygon/add")
    public void add(@RequestParam(value="coords") String list)
    {
        System.out.println(list);
        try {
            RKPolygon polygon = new RKPolygon(list);
            polygon.saveToDB();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    @RequestMapping("/adv/add")
    private void addAdv(@RequestParam(value="brandName") String brandName,
                        @RequestParam(value="allInfo") String allInfo,
                        @RequestParam(value="advType") String advType,
                        @RequestParam(value="advHeader") String advHeader,

                        @RequestParam(value="beginInner") String beginInner,
                        @RequestParam(value="endInner") String endInner,
                        @RequestParam(value="beginOfficial") String beginOfficial,
                        @RequestParam(value="showBeginOfficial") boolean showBeginOfficial,
                        @RequestParam(value="endOfficial") String endOfficial,
                        @RequestParam(value="showEndOfficial") boolean showEndOfficial,

//                        @RequestParam(value="mallName") String mallName,
                        @RequestParam(value="mallId") String mallId,
//                        @RequestParam(value="latitude") Double latitude,
//                        @RequestParam(value="longitude") Double longitude,

                        @RequestParam(value="coordinatesWithinMall") String coordinatesWithinMall,
                        @RequestParam(value="link") String link,
                        @RequestParam(value="lentaBannerUrl") String lentaBannerUrl,
                        @RequestParam(value="mainBannerUrl") String mainBannerUrl,
                        @RequestParam(value="tags") String tags) throws Exception
    {
        System.out.println("@RequestMapping(\"/adv/add\")");

        AdvInfo.AdvTypes _atype = null;
        System.out.println(advType);
        if (advType.equals("sale"))
            _atype = AdvInfo.AdvTypes.sale;
        else if (advType.equals("newcollection"))
            _atype = AdvInfo.AdvTypes.newcollection;
        else
            throw new Exception("Adv type is not sale or newcollection: [" + advType + "]");
        AdvInfo advInfo = new AdvInfo(brandName, allInfo, _atype, advHeader);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        AdvDate advDate = new AdvDate(format.parse(beginInner), format.parse(endInner), format.parse(beginOfficial), showBeginOfficial, format.parse(endOfficial), showEndOfficial);

        SQliteManager sQliteManager = SQliteManager.getInstance();
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();

//        AdvMall advMall = sQliteManager.getAdvMall(mallId);
        AdvMall advMall = mongoDBManager.getAdvMall(mallId);

        AdvLinks advLinks = new AdvLinks(coordinatesWithinMall, link, lentaBannerUrl, mainBannerUrl);

        Adv adv = new Adv(advInfo, advDate, advMall, advLinks, tags);

        System.out.println("before adv.saveToDB()");
        adv.saveToDB();
    }

    @RequestMapping("/adv/{id}") //__5__
    public void segueIncrease(@PathVariable("id") String id)
    {
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        mongoDBManager.increaseAdvViewCount(id);
    }

    @RequestMapping(value = "/request", method = RequestMethod.POST) //__6__
    public void saveUserRequest(@RequestParam(value="request") String request,
                                @RequestParam(value="latitude") double latitude,
                                @RequestParam(value="longitude") double longitude)
    {
        System.out.printf("Request mapping: Request[%s] lat[%f] long[%f]\n", request, latitude, longitude);
        MongoDBManager mongoDBManager = MongoDBManager.getInstance();
        Date now = new Date();
        mongoDBManager.saveUserRequest(request, latitude, longitude, now);
    }


//    @RequestMapping("/point/add/newcome")
//    public void addNewcome(@RequestParam(value="latitude") double latitude,
//                    @RequestParam(value="longitude") double longitude,
//                    @RequestParam(value="radius") double radius,
//                    @RequestParam(value="name") String name,
//                    @RequestParam(value="info") String info,
//                    @RequestParam(value="header") String header,
//                    @RequestParam(value="imageurl") String imageUrl,
//                    @RequestParam(value="banerurl") String banerUrl)
//    {
//        try{
//            ShopInfo shopInfo = new ShopInfo(name, info, header, imageUrl, banerUrl, "newcome");
//            RKPoint point = new RKPoint(latitude, longitude, radius, shopInfo);
//            point.saveToDB();
//        }catch (Exception e) {
//            System.out.println(e.toString());
//        }
//    }
//
//    @RequestMapping("/point/add/disc")
//    public void addDiscount(@RequestParam(value="latitude") double latitude,
//                    @RequestParam(value="longitude") double longitude,
//                    @RequestParam(value="radius") double radius,
//                    @RequestParam(value="name") String name,
//                    @RequestParam(value="info") String info,
//                    @RequestParam(value="header") String header,
//                    @RequestParam(value="imageurl") String imageUrl,
//                    @RequestParam(value="banerurl") String banerUrl)
//    {
//        try{
//            ShopInfo shopInfo = new ShopInfo(name, info, header, imageUrl, banerUrl, "discount");
//            RKPoint point = new RKPoint(latitude, longitude, radius, shopInfo);
//            point.saveToDB();
//        }catch (Exception e) {
//            System.out.println(e.toString());
//        }
//    }
}
