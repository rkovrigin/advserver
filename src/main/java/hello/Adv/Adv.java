package hello.Adv;

/**
 * Created by romankovrigin on 13.04.16.
 */

//import com.fasterxml.jackson.databind.ObjectMapper;
import hello.DBMS.MongoDBManager;
import hello.DBMS.SQliteManager;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.ArrayList;
import java.util.UUID;

public class Adv {
    public AdvInfo advInfo;
    public AdvDate advDate;
    public AdvMall advMall;
    public AdvLinks advLinks;
    public String uid;
    public String[] tags;

    public Adv(AdvInfo advInfo, AdvDate advDate, AdvMall advMall, AdvLinks advLinks, String tags) throws Exception
    {
        this.advInfo = advInfo;
        this.advDate = advDate;
        this.advMall = advMall;
        this.advLinks = advLinks;
        this.uid = UUID.randomUUID().toString();
        this.tags = tags.split(",");
    }

    public void saveToDB()
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
            mapper.disable(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS);

            String adv = mapper.writeValueAsString(this);
//            mapper.generateJsonSchema();
            System.out.println("saveToDB:\n" + adv);
//            SQliteManager sQliteManager = SQliteManager.getInstance();
//            sQliteManager.addCoord(adv, this.advInfo.type.toString());

            MongoDBManager mongoDBManager = MongoDBManager.getInstance();
            mongoDBManager.saveObject(this);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }
}
