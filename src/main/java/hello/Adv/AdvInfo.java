package hello.Adv;

/**
 * Created by romankovrigin on 13.04.16.
 */

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonFormat;

public class AdvInfo {
    public static enum AdvTypes {sale, newcollection};
    @JsonView
    public String brandName;
    @JsonView
    public String allInfo;
    @JsonView
    public AdvTypes type;
    @JsonView
    public String header;
    public AdvInfo(String brandName, String allInfo, AdvTypes type, String header)
    {
        this.brandName = brandName;
        this.allInfo = allInfo;
        this.type = type;
        this.header = header;
    }
}
