package hello.Adv;

/**
 * Created by romankovrigin on 13.04.16.
 */

import hello.Figures.Coordinates;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonFormat;

public class AdvMall {
    @JsonView
    public String mall;
    @JsonView
    public Coordinates coordinates;
    public AdvMall(String mall, Coordinates coordinates)
    {
        this.mall = mall;
        this.coordinates = coordinates;
    }
}
