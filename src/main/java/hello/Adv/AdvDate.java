package hello.Adv;

/**
 * Created by romankovrigin on 13.04.16.
 */

import java.util.Date;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonFormat;

public class AdvDate {
    @JsonView
    public Date beginInner;
    @JsonView
    public Date endInner;
    @JsonView
    public Date beginOfficial;
    @JsonView
    public boolean showBeginOfficial;
    @JsonView
    public Date endOfficial;
    @JsonView
    public boolean showEndOfficial;
    public AdvDate(Date beginInner, Date endInner, Date beginOfficial, boolean showBeginOfficial, Date endOfficial, boolean showEndOfficial) throws Exception
    {
        Exception innerDatesException = new Exception("Dates: endInner date is less than beginInner");
        if (beginInner.after(endInner))
            throw  innerDatesException;

        Exception officialDatesException = new Exception("Dates: endOfficial date is less than beginOfficial");
        if (beginOfficial.after(endOfficial))
            throw officialDatesException;

        this.beginInner = beginInner;
        this.endInner = endInner;
        this.beginOfficial = beginOfficial;
        this.showBeginOfficial = showBeginOfficial;
        this.endOfficial = endOfficial;
        this.showEndOfficial = showEndOfficial;
    }
}
