package hello.Adv;

/**
 * Created by romankovrigin on 13.04.16.
 */

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonFormat;

public class AdvLinks {
    @JsonView
    public String coordinatesWithinMall;
    @JsonView
    public String officialWebSite;
    @JsonView
    public String lentaBanerUrl;
    @JsonView
    public String mainBannerUrl;

    public AdvLinks(String coordinatesWithinMall, String officialWebSite, String lentaBanerUrl, String mainBannerUrl)
    {
        this.coordinatesWithinMall = coordinatesWithinMall;
        this.officialWebSite = officialWebSite;
        this.lentaBanerUrl = lentaBanerUrl;
        this.mainBannerUrl = mainBannerUrl;
    }
}
