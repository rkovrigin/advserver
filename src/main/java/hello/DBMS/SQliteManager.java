package hello.DBMS;
import hello.Adv.AdvMall;
import hello.Coord;
import hello.Figures.Coordinates;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by romankovrigin on 05.03.16.
 */
public class SQliteManager {

    public static SQliteManager getInstance() {return sQliteManager;}
    public static String databaseName = "coord.db";
    private static SQliteManager sQliteManager = new SQliteManager();
    private SQliteManager()
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            File dbFile = new File(databaseName);
            if (!dbFile.exists())
            {
                this.createDB();
            }
            this.createCoordTable();
        }
        catch (Exception e)
        {
            System.out.println("SQliteManager() " + e.toString());
        }
    }

    private void createDB()
    {
        try
        {
            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:sqlite:"+databaseName);
            connection.close();
        }catch (Exception e)
        {
            System.out.format("SQLite problem %s\n", e.toString());
        }
    }

    private Connection getConnection()
    {
        Connection connection = null;
        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:"+databaseName);
        }
        catch (Exception e)
        {
            System.out.format("SQLite problem %s\n", e.toString());
        }
        return connection;
    }

    private void createCoordTable()
    {
        Connection connection = this.getConnection();
        Statement stmt = null;
        try
        {
            stmt = connection.createStatement();
            String sql = "CREATE TABLE coord " +
                    "(ID INTEGER PRIMARY KEY     AUTOINCREMENT," +
                    "Object TEXT                 NOT NULL," +
                    "Type VARCHAR(24)            NOT NULL);";
            stmt.executeUpdate(sql);
            stmt.close();
            connection.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }
    public boolean addCoord(String figure, String type) throws SQLException {
        Connection c = this.getConnection();
        PreparedStatement pstmt = null;
        try {
            pstmt = c.prepareStatement("INSERT INTO coord (Object, Type) VALUES (?, ?)");
            pstmt.setString(1, figure);
            pstmt.setString(2, type);
            System.out.println(pstmt.toString());
            pstmt.executeUpdate();
        }catch (Exception e){
            System.out.println(e.toString());
            return false;
        }
        pstmt.close();
        c.close();
        System.out.println("Coord " + figure + " added!");
        return true;
    }

    public ArrayList<String> selectAll() throws SQLException {
        ArrayList<String> figures = new ArrayList<String>();
        Connection c = this.getConnection();
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            String sql = "SELECT * from coord;";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                String figure = rs.getString("Object");
                figures.add(figure);
            }
        }catch (Exception e){
            System.out.println(e.toString());
        }
        stmt.close();
        c.close();
        return figures;
    }

    public ResultSet getResultSet(String query) throws SQLException {
        Connection c = this.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = c.createStatement();
            rs = stmt.executeQuery( query );
        }catch (Exception e){
            System.out.println(e.toString());
        }
        stmt.close();
        c.close();
        return rs;
    }

    public JSONArray getAllMalls() throws SQLException
    {
        String query = "select * from mall";
        String lat = "latitude";
        String lon = "longitude";
        Connection connection = this.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
//        ArrayList<String> jarray = new ArrayList<String>();
        JSONArray ja = new JSONArray();
        try
        {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                try {
                    String mallName = rs.getString("name");
                    int mallId = rs.getInt("id");
                    double latitude = rs.getDouble(lat);
                    double longitude = rs.getDouble(lon);
                    JSONObject jo = new JSONObject();
                    JSONObject jco = new JSONObject();
                    jo.put("mall", mallName);
                    jo.put("id", mallId);
                    jco.put(lat, latitude);
                    jco.put(lon, longitude);
                    jo.put("coordinates", jco);
//                    jarray.add(jo.toString());
                    ja.put(jo);
                    System.out.println(jo);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        stmt.close();
        connection.close();

        return ja;
    }

    public AdvMall getAdvMall(Integer mallId) throws SQLException {
        Connection connection = this.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        String query = "select * from mall where id = " + mallId.toString();
        Coordinates mallCoordinates = null;
        String mallName = null;
        try
        {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next())
            {
                String lat = "latitude";
                String lon = "longitude";
                mallCoordinates = new Coordinates(rs.getDouble(lat), rs.getDouble(lon));
                mallName = rs.getString("name");
            }
            else
            {
                throw new Exception("No mall for id " + mallId.toString());
            }
        }catch (Exception e) {
            System.out.println(e.toString());
        }
        finally {
            stmt.close();
            rs.close();
        }

        AdvMall advMall = new AdvMall(mallName, mallCoordinates);

        return advMall;
    }

    public ArrayList<String> selectQuery(String query) throws SQLException {
        ArrayList<String> figures = new ArrayList<String>();
        Connection c = this.getConnection();
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( query );
            while (rs.next()){
                String figure = rs.getString("Object");
                figures.add(figure);
            }
        }catch (Exception e){
            System.out.println(e.toString());
        }
        stmt.close();
        c.close();

        return figures;
    }

    public Coord selectCoordWithId(int id) throws SQLException {
        Coord coord = null;
        Connection c = this.getConnection();
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            String sql = String.format("SELECT * from coord where ID = %d;", id);
            ResultSet rs = stmt.executeQuery( sql );
            double longitude = rs.getDouble("Longitude");
            double latitude = rs.getDouble("Latitude");
            int radius = rs.getInt("Radius");
            coord = new Coord(id, longitude, latitude, radius);
        }catch (Exception e){
            System.out.println(e.toString());
        }
        stmt.close();
        c.close();

        return coord;
    }
}
