package hello.DBMS;

/**
 * Created by romankovrigin on 14.04.16.
 */

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.util.JSON;
import com.sun.org.apache.xerces.internal.impl.dv.xs.DateDV;
import hello.Adv.Adv;
import hello.Adv.AdvMall;
import hello.Figures.Coordinates;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


public class MongoDBManager
{
    public static MongoDBManager getInstance() {return mongoDBManager;}
    public static String databaseName = "coordmongo";
    private static String collectionAdvert = "advert";
    private static String collectionMall = "mall";
    private static String collectionCity = "city";
    private static String collectionUserRequest = "userrequest";
    private static MongoDBManager mongoDBManager = new MongoDBManager();
    public static MongoDatabase db;

    public MongoDBManager()
    {
        try
        {
            MongoClient mongoClient = new MongoClient("localhost", 27017);
            this.db = mongoClient.getDatabase(this.databaseName);
            if (!isCollectionExist(collectionAdvert))
                this.db.createCollection(collectionAdvert);
            if (!isCollectionExist(collectionMall))
                createMallCollection();
            if (isCollectionExist(collectionCity))
                this.db.createCollection(collectionCity);
            if (isCollectionExist(collectionUserRequest))
                this.db.createCollection(collectionUserRequest);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private boolean isCollectionExist(String collection)
    {
        MongoIterable<String> cols= this.db.listCollectionNames();
        for (String col: cols)
        {
            if (col.equals(collection)) {
                System.out.println("Collection " + collection + " exists!");
                return true;
            }
        }
        System.out.println("Collection " + collection + " doesn't exist!");
        return false;
    }

    private void createMallCollection()
    {
        this.db.createCollection(collectionMall);
        Document docMallGallery = new Document();
        docMallGallery.append("mall", "Gallery").append("latitude", 59.927301).append("longitude", 30.360396);
        Document docMallVladimirsky = new Document();
        docMallVladimirsky.append("mall", "Vladimirsky").append("latitude", 59.928516).append("longitude", 30.346706);

        this.db.getCollection(collectionMall).insertOne(docMallGallery);
        this.db.getCollection(collectionMall).insertOne(docMallVladimirsky);

        FindIterable<Document> iterable = db.getCollection(collectionMall).find(new Document());

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson().toString());
            }
        });
    }

    public void saveObject(Adv adv) throws IOException {
        System.out.println("saveObject");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
        mapper.disable(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS);

        Document mongo_adv = new Document();
        DBObject advInfo = (DBObject) JSON.parse(mapper.writeValueAsString(adv.advInfo));
        DBObject advDate = (DBObject) JSON.parse(mapper.writeValueAsString(adv.advDate));
        DBObject advMall = (DBObject) JSON.parse(mapper.writeValueAsString(adv.advMall));
        DBObject advLinks = (DBObject) JSON.parse(mapper.writeValueAsString(adv.advLinks));
        DBObject advTags = (DBObject) JSON.parse(mapper.writeValueAsString(adv.tags));
        mongo_adv.append("advInfo", advInfo);
        mongo_adv.append("advDate", advDate);
        mongo_adv.append("advMall", advMall);
        mongo_adv.append("advLinks", advLinks);
        mongo_adv.append("advTags", advTags);

        db.getCollection(collectionAdvert).insertOne(mongo_adv);
        System.out.printf("object saved: %s", mongo_adv.toJson().toString());
//        db.getCollection(collectionAdvert).drop();

//        FindIterable<Document> iterable = db.getCollection(collectionAdvert).find(new Document());
//
//        iterable.forEach(new Block<Document>() {
//            @Override
//            public void apply(final Document document) {
//                System.out.println(document.toJson().toString());
//            }
//        });
    }

    private ArrayList<Document> getCollectionDocs(String collection)
    {
        FindIterable<Document> iterable = db.getCollection(collection).find(new Document());
        final ArrayList<Document> objects = new ArrayList<Document>();

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson().toString());
                objects.add(document);
            }
        });

        return objects;
    }

    private ArrayList<Document> getCollectionDocs(String collection, String key, ObjectId id)
    {
        FindIterable<Document> iterable = db.getCollection(collection).find(new Document(key, id));
        final ArrayList<Document> objects = new ArrayList<Document>();

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson().toString());
                objects.add(document);
            }
        });

        return objects;
    }

    private ArrayList<Document> getCollectionDocs(String collection, String key0, ObjectId id0, String key1, ObjectId id1)
    {
        BasicDBObject getQuery = new BasicDBObject();
        getQuery.append(key0, id0);
        getQuery.append(key1, id1);
        FindIterable<Document> iterable = db.getCollection(collection).find(getQuery);
        final ArrayList<Document> objects = new ArrayList<Document>();

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson().toString());
                objects.add(document);
            }
        });

        return objects;
    }

    public ArrayList<Document> getAdverts()
    {
        return getCollectionDocs(collectionAdvert);
    }

    public ArrayList<Document> getAdvertsAtCity(String cityid, String advid)
    {
        if (advid.isEmpty())
            return getCollectionDocs(collectionAdvert, "city.id", new ObjectId(cityid));
        return getCollectionDocs(collectionAdvert, "city.id", new ObjectId(cityid), "advMall.id", new ObjectId(advid));
    }

    public ArrayList<Document> getMalls()
    {
        return getCollectionDocs(collectionMall);
    }

    public ArrayList<Document> getMalls(String cityid)
    {
        return getCollectionDocs(collectionMall, "city.id", new ObjectId(cityid));
    }

    public ArrayList<Document> getCities()
    {
        return getCollectionDocs(collectionCity);
    }

    public AdvMall getAdvMall(String mallId)
    {
        FindIterable<Document> iterable = db.getCollection(collectionMall).find(new Document("_id", new ObjectId(mallId)));

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println("FOUND MALL with ID");
                System.out.println(document.toJson().toString());
            }
        });
        Document d = iterable.first(); //add emptiness check
        return new AdvMall(d.getString("mall"), new Coordinates(d.getDouble("latitude"),d.getDouble("longitude")));
    }

    public void increaseAdvViewCount(final String advID)
    {
        MongoCollection col = db.getCollection(collectionAdvert);
        BasicDBObject increment = new BasicDBObject().append("$inc", new BasicDBObject("feedback.segue", 1));
        UpdateResult result = col.updateOne(new BasicDBObject().append("_id", new ObjectId(advID)), increment);
        System.out.println("Increase feedback.segue with id: " + advID + "; Result: " + result.toString());
    }

    public void saveUserRequest(final String request, final double latitude, final double longitude, Date now)
    {
        Document searchWords = new Document();
        searchWords.append("request", request).append("latitude", latitude).append("longitude", longitude).append("date", now);

        this.db.getCollection(collectionUserRequest).insertOne(searchWords);
        System.out.printf("saveUserRequest: Request[%s] lat[%f] long[%f] " + now.toString() + "\n" , request, latitude, longitude);
    }
}
