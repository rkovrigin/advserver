package hello;

import com.fasterxml.jackson.annotation.JsonView;

public class Coord {
    @JsonView(View.Summery.class)
    private int id;
    @JsonView(View.Summery.class)
    private final double longitude;
    @JsonView(View.Summery.class)
    private final double latitude;
    @JsonView(View.Summery.class)
    private final int radius;

    public Coord(int id, double longitude, double latitude, int radius){
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
    }
}
